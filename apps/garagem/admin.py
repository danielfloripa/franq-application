from django.contrib import admin
from apps.garagem.models import Garagem, Veiculo


class GaragemVeiculoAdminInline(admin.StackedInline):
    model = Veiculo

    def get_extra(self, request, obj=None, **kwargs):
        extra = 1
        if obj:
            garagem = self.opts.model.objects.filter(pk=obj.id)
            return 0 if len(garagem) else extra
        return extra


class GaragemAdmin(admin.ModelAdmin):
    list_display = ('id', 'email', 'telefone', 'numero_de_carros')
    inlines = [GaragemVeiculoAdminInline]

    def numero_de_carros(self, obj):
        garagens = Veiculo.objects.filter(garagem=obj)
        return len(garagens)


class VeiculoAdmin(admin.ModelAdmin):
    list_display = ('id', 'tipo', 'modelo', 'cor', 'ano', 'garagem')

    def garagem(self, obj):
        garagens = Garagem.objects.filter(pk=obj.id)
        return len(garagens)


admin.site.register(Garagem, GaragemAdmin)
admin.site.register(Veiculo, VeiculoAdmin)
