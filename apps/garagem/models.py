from django.db import models

from apps.pessoa.models import Pessoa


class Garagem(models.Model):
    pessoa_owner = models.ForeignKey(Pessoa, on_delete=models.CASCADE)
    associado = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def email(self):
        if self.pessoa_owner:
            return self.pessoa_owner.email

    def telefone(self):
        if self.pessoa_owner:
            return self.pessoa_owner.telefone

    def clean(self):
        if self.pessoa_owner and self.id and not self.associado:
            Garagem.objects.filter(id=self.id).delete()

    def __str__(self):
        return str(self.id) if self.id else "0"

    class Meta:
        verbose_name_plural = 'garagens'
        db_table = 'garagem'


class Veiculo(models.Model):
    garagem = models.ForeignKey(Garagem, on_delete=models.CASCADE, default="", blank=True, null=True)
    tipo = models.CharField(max_length=1, choices=(('M', 'Moto'), ('C', 'Carro',),))
    modelo = models.CharField(max_length=64)
    cor = models.CharField(max_length=16)
    ano = models.IntegerField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f"{self.tipo}: {self.modelo} / {self.cor} / {self.ano}"

    class Meta:
        verbose_name_plural = 'veiculos'
        db_table = 'veiculo'
