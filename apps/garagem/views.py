from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated

from apps.garagem.models import Garagem, Veiculo
from apps.pessoa.models import Pessoa
from apps.token_auth import TokenAuthSupportQueryString


class GaragemAPI(APIView):
    permission_classes = (IsAuthenticated,)
    authentication_classes = (TokenAuthSupportQueryString,)

    def get(self, request, *args, **kwargs):
        code = 200
        if kwargs.get('id'):
            try:
                garagem = Garagem.objects.get(id=kwargs.get('id'))
                content = {
                    "success": True,
                    "data": self.extract(garagem)}
            except Exception:
                code = 404
                content = {"success": False, 'data': {'id': 'not found'}}
        else:
            garagens = Garagem.objects.all()
            content = []
            for garagem in garagens:
                content.append({
                    "success": True,
                    "data": self.extract(garagem)})
        return Response(content, status=code)

    def post(self, request):
        dados = request.data
        if 'pessoa_owner' in dados.keys():
            try:
                pessoa = Pessoa.objects.get(email=dados['pessoa_owner'])
            except Exception as e:
                return Response({'message': "Pessoa não cadastrada!"})
        dados['pessoa_owner'] = pessoa
        garagem = Garagem(**dados)
        garagem.save()
        content = {'message': f'Adicionada a {garagem} para a pessoa {pessoa.email}!'}
        return Response(content)

    @staticmethod
    def extract(garagem):
        return {
            "garagem_id": garagem.id,
            "pessoa_id": garagem.pessoa_owner_id,
            "associado": garagem.associado,
            "created_at": garagem.created_at,
            "updated_at": garagem.updated_at
        }


class VeiculoAPI(APIView):
    permission_classes = (IsAuthenticated,)
    authentication_classes = (TokenAuthSupportQueryString,)


    def get(self, request, *args, **kwargs):
        code = 200
        if kwargs.get('id'):
            try:
                veiculo = Veiculo.objects.get(id=kwargs.get('id'))
                content = {
                    "success": True,
                    "data": self.extract(veiculo)}
            except Exception:
                code = 404
                content = {"success": False, 'data': {'id': 'not found'}}
        else:
            veiculos = Veiculo.objects.all()
            content = []
            for veiculo in veiculos:
                content.append({
                    "success": True,
                    "data": self.extract(veiculo)})
        return Response(content, status=code)

    def post(self, request):
        warning = ""
        dados = request.data
        garagem_id = dados.pop('garagem') if dados.get('garagem') else None
        veiculo = Veiculo(**dados)

        if garagem_id:
            try:
                exists_garagem = Garagem.objects.get(id=garagem_id)
                veiculo.garagem = exists_garagem
            except Exception as e:
                warning = f" Mas a garagem {garagem_id} não existe e precisa ser atribuída posteriormente"
        veiculo.save()
        content = {'message': f'Adicionado veiculo {veiculo}!{warning}'}
        return Response(content)

    @staticmethod
    def extract(veiculo):
        return {
            "veiculo_id": veiculo.id,
            "garagem": veiculo.garagem_id,
            "tipo": veiculo.tipo,
            "modelo": veiculo.modelo,
            "cor": veiculo.cor,
            "ano": veiculo.ano,
            "created_at": veiculo.created_at,
            "updated_at": veiculo.updated_at
        }