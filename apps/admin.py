from django.contrib import admin

from apps.garagem.models import Garagem
from apps.pessoa.models import Pessoa, User
from apps.pessoa.views import PessoaAPI


#class UserPessoaAdminInline(admin.StackedInline):
#    model = User


class PessoaGaragemAdminInline(admin.StackedInline):
    model = Garagem
    show_change_link = True

    def get_extra(self, request, obj=None, **kwargs):
        extra = 1
        if obj:
            garagem = self.opts.model.objects.filter(pessoa_owner_id=obj)
            return 0 if len(garagem) else extra
        return extra

    def get_queryset(self, request, *args, **kwargs):
        if request.POST:
            try:
                self.update_pessoa_garagem(request)
            except Exception as e:
                self.add_user_pessoa_garagem(request)
        queryset = super().get_queryset(request)
        if not self.has_view_or_change_permission(request):
            queryset = queryset.none()
        return queryset

    def update_pessoa_garagem(self, request):
        is_deleted = any(["DELETE" in key for key in request.POST.keys()])
        pessoa = Pessoa.objects.get(email=request.POST.get('email'))
        if request.POST.get('adicionar_garagem') == 'on' and not is_deleted:
            for key in request.POST.keys():
                for i in range(int(request.POST.get('garagem_set-TOTAL_FORMS'))):
                    if f'garagem_set-{i}-id' == key and request.POST.get(key) == "" and request.POST.get(key.replace('id', 'associado')) == 'on':
                        self.opts.model(pessoa_owner=pessoa, associado=True).save()
        else:
            if pessoa:
                garagem = self.opts.model.objects.filter(pessoa_owner_id=pessoa)
                ids = list(garagem.values_list('id', flat=True))
                if is_deleted:
                    ids = [int(request.POST.get(key.replace("DELETE", 'id'))) for key in request.POST.keys() if "DELETE" in key]
                _ = [g.delete() for g in garagem if g.id in ids]

    def add_user_pessoa_garagem(self, request):
        from copy import copy
        request.data = copy(request.POST)
        request.data.update({'password': request.data.get('telefone')})
        pessoa = PessoaAPI().post(request, user_exists=True)
        return True if pessoa.status_code == 200 else False


class PessoaAdmin(admin.ModelAdmin):

    list_display = ('user', 'nome', 'email', 'id_garagens',)
    search_fields = ['user','nome', 'email']
    inlines = [PessoaGaragemAdminInline]

    fieldsets = (
        (None, {
            'fields': ('nome', 'email', 'telefone', 'tipo', 'adicionar_garagem')
        }),
        ('Adicione um usuário para acesso', {
            'classes': ('wide', 'extrapretty'), #('collapse',),
            'fields': ('user',),
        }),
    )

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == "car":
            kwargs["queryset"] = Garagem.objects.filter(pessoa_owner_id=request.user)
        return super().formfield_for_foreignkey(db_field, request, **kwargs)

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        if request.user.is_superuser:
            return qs
        r = qs.filter(nome=request.user)
        #garagem = Garagem(pessoa_owner_id='xxx', associado=True)
        #garagem.save()
        return r

    def id_garagens(self, obj):
        garagens = Garagem.objects.filter(pessoa_owner_id=obj)
        return [g.id for g in garagens] if garagens else ['--']

    def add_garagem(self, obj):
        garagem = Garagem(pessoa_owner=obj, associado=True)
        garagem.save()
