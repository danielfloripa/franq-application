from django.contrib import admin

from apps.admin import PessoaAdmin
from apps.pessoa.models import Pessoa


class PessoaInline(admin.StackedInline):
    model = Pessoa
    can_delete = False


admin.site.register(Pessoa, PessoaAdmin)

