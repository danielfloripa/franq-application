from rest_framework.views import APIView
from rest_framework.response import Response
from django.contrib.auth.models import User

from apps.pessoa.models import Pessoa
from apps.garagem.models import Garagem


class PessoaAPI(APIView):

    def get(self, request, *args, **kwargs):
        code = 200
        if kwargs.get('id'):
            try:
                pessoa = Pessoa.objects.get(user_id=kwargs.get('id'))
                content = {
                    "success": True,
                    "data": self.extract(pessoa)}
            except Exception:
                code = 404
                content = {"success": False, 'data': {'id': 'not found'}}
        else:
            pessoas = Pessoa.objects.all()
            content = []
            for pessoa in pessoas:
                content.append({
                    "success": True,
                    "data": self.extract(pessoa)})
        return Response(content, status=code)

    def post(self, request, *args, **kwargs):
        garagem = None
        keys = ('nome', 'telefone', 'email', 'adicionar_garagem', 'tipo', 'password', 'user')
        dados = {k: v for k, v in request.data.items() if k in keys}
        dados['adicionar_garagem'] = True if dados.get('adicionar_garagem') in ('on', '1', 1, 'true') else False
        if not kwargs.get('user_exists'):
            try:
                user = User.objects.create(username=dados.get('email', dados.get('username')))
            except Exception as e:
                return Response({"message": str(e)})
            user.set_password(dados.pop('password'))
            user.save()
        else:
            _ = dados.pop('password')
            user = User.objects.get(id=dados.get('user'))
        dados.update({'user': user})
        try:
            pessoa = Pessoa.objects.get(user=user)
            pessoa.nome = dados.get('nome')
            pessoa.email = dados.get('email')
            pessoa.tipo = dados.get('tipo')
            pessoa.adicionar_garagem = dados.get('adicionar_garagem')
            pessoa.telefone = dados.get('telefone')
            pessoa = self.update_user_profile(user, pessoa)
        except Exception as e:
            pessoa = Pessoa(**dados)
        pessoa.save()
        if dados.get('adicionar_garagem', True):
            garagem = Garagem(pessoa_owner=pessoa, associado=True)
            garagem.save()
        content = {'message': f'Created with user_id: {pessoa.user_id} and garagem_id {garagem.id if garagem else 0} '}
        return Response(content)

    def delete(self, request, *args, **kwargs):
        code = 400
        content = {}
        if kwargs.get('id'):
            try:
                user = User.objects.get(id=kwargs.get('id'))
                deleted_objects = user.delete()
                code = 200
                content = {'success': True,
                           'deleted_objects': deleted_objects[0],
                           'details': deleted_objects[1]}
            except Exception as e:
                content = {
                    'success': False,
                    'deleted_objects': 0,
                    'details': str(e)
                }
        return Response(content, status=code)

    def validate_request(self, data):
        keys_validate = ('nome', 'telefone', 'email', 'tipo', 'password')
        if any(key in keys_validate for key in data.keys()):
            return data
        raise

    def extract(self, pessoa):
        return dict(
            user_id=pessoa.user_id,
            nome=pessoa.nome,
            telefone=pessoa.telefone,
            email=pessoa.email,
            tipo=pessoa.tipo,
            adicionar_garagem=pessoa.adicionar_garagem,
            created_at=pessoa.created_at,
            updated_at=pessoa.updated_at
        )

    @staticmethod
    def update_user_profile(user, pessoa):
        names = pessoa.nome.split(" ")
        if len(names) > 1:
            user.first_name = names[0]
            user.last_name = " ".join(names[1:])
        else:
            user.first_name = names[0]
        user.email = pessoa.email
        user.is_staff = True
        admin = True if pessoa.tipo == 'A' else False
        if admin:
            user.is_superuser = True
        user.save()
        return pessoa
