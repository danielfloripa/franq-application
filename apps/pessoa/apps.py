from django.apps import AppConfig


class PessoaConfig(AppConfig):
    name = 'apps.pessoa'
