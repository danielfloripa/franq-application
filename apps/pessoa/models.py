from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.conf import settings


class Pessoa(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name='profile', null=True)
    nome = models.CharField(max_length=128)
    telefone = models.CharField(max_length=20)
    email = models.EmailField(max_length=32, primary_key=True, blank=False)
    tipo = models.CharField(max_length=15, choices=(('C', 'Consumidor',), ('A', 'Administrador',),)) #models.ForeignKey(PessoaTipo, on_delete=models.DO_NOTHING)
    adicionar_garagem = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f'{self.email}'

    class Meta:
        verbose_name_plural = 'pessoas'
        db_table = 'pessoa'

    def get_email(self):
        return self.email

    def get_telefone(self):
        return self.telefone

    def ids_das_garagem(self, obj):
        from apps.garagem.models import Garagem
        garagens = Garagem.objects.filter(pessoa_owner_id=obj.email)
        return [g.id for g in garagens] if garagens else ['--']


@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        try:
            Pessoa.objects.get(email=instance)
        except Exception as e:
            Pessoa.objects.create(email=instance.username)


@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    try:
        pessoa = instance.profile
        pessoa.nome = f"{instance.first_name} {instance.last_name}"
        pessoa.email = instance.email
        pessoa.tipo = 'A' if instance.is_superuser else 'C'
        pessoa.save()
    except Exception as e:
        return
