"""core URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.utils.translation import gettext_lazy
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework import parsers, renderers
from apps.garagem import views as g_views
from apps.pessoa import views as p_views

admin.site.site_title = gettext_lazy('Franq')
site_header = gettext_lazy('Administração Franq Application ')
index_title = gettext_lazy('Administração')

ObtainAuthToken.renderer_classes = (renderers.JSONRenderer, renderers.BrowsableAPIRenderer, )

urlpatterns = [
    path('admin/', admin.site.urls),

    path('garagem', g_views.GaragemAPI.as_view(), name='Garagem'),
    path('garagem/auth_token', g_views.GaragemAPI.as_view(), name='Garagem'),
    path('garagem/<id>', g_views.GaragemAPI.as_view(), name='Garagem'),

    path('veiculo', g_views.VeiculoAPI.as_view(), name='Veiculo'),
    path('veiculo/<id>', g_views.VeiculoAPI.as_view(), name='Veiculo'),

    path('pessoa', p_views.PessoaAPI.as_view(), name='Pessoas'),
    path('pessoa/<id>', p_views.PessoaAPI.as_view(), name='Pessoa'),

    path('get_token', ObtainAuthToken.as_view(), name='Get Token Auth')

]
