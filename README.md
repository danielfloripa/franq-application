# FRANQ Application (Software Engineer)

## Descrição

API desenvolvida em Python / Django que permite o gerenciamento de pessoas, garagens e veículos.

É utilizado o painel de administração do Django para visualizar os objetos relacionados.

Adicionalmente, é disponibilizada uma REST-API (Django REST framework) para realizar o mesmo gerenciamento desses objetos, mas para aplicações externas.

## Rquisitos do ambiente ( mais detalhes em requirements.txt)

* Python 3.7 ou superior
* Django 3 ou superior
* Django REST framework 3  ou superior
* Base Postgres por padrão

## Instalação

Executar os comandos (Linux)

```bash
$ git clone git@bitbucket.org:danielfloripa/franq-application.git
$ cd franq-application
$ python3.7 -m venv venv
$ source venv/bin/activate
$ pip3 install -r requirements.txt
```

Arquivo .env está com as credenciais do Heroku (PS: não colocaria os dados de uma base se não fosse de testes).

Se for adicionar uma nova base, devem ser alteradas as credenciais no arquivo `.env`, depois, executar os comandos: 

```bash
$ python manage.py makemigrations
$ python manage.py migrate
$ python manage.py createsuperuser --username <SEU_USERNAME> --email <SEU_EMAIL>
```

## Execução

Para executar o serviço:

```bash
$ python manage.py runserver localhost:8000
```

## Acessos/Segurança

É utilizada a segurança padrão para acesso ao painel, com base na autenticação de usuário/senha.

A segurança da REST-API é realizada através de token (AuthToken do rest_framework), que pode ser adicionado como complemento na url desejada (para visualização do REST Framework)
 ou na header para acesso via backend.
 
 
## Mapeamento de URLs:

O serviço está hospedado no Heroku:

URL: <https://franq-app.herokuapp.com/>

Mas se a execução for local, considerar a URL como <http://127.0.0.1:8000/>

### 1. Painel:

Administração: <http://URL/admin/>

No painel, haverá os links dos objetos após o /admin (exemplo <http://URL/admin/pessoa>)

* Usuáro padrão: franq
* Senha padrão: franq@2021

### 2. REST API Pessoa:

Pessoa: <http://URL/pessoa>

Enviar um JSON de exemplo:

```json
{
    "nome": "Sou Franq",
    "telefone": "2345678",
    "email": "franq@franq.com",
    "tipo": "A",
    "adicionar_garagem": true,
    "password": "abde12345"
}
```

Será retornado o id da pessoa e da garagem

Após criar a 'Pessoa', é criado o 'User' automaticamente (o mesmo que acessa o painel)

### 3. REST API Gerar token:

Para acessar a 'Garagem' e 'Veículo', é necessário gerar um token (no painel ou no endpoint):

TOKEN: <http://URL/get_token>

### 4. REST API Garagem e Veículo:

Acessar os serviços com o token, com o uso de query strings (`?auth_token=`)

Garagem: <http://URL/garagem?auth_token=3674d78d158b6b252fd99074bf759aa0e7208b78>

```json
{
    "pessoa_owner": "franq@franq.com"
}
```

Veículo: <http://URL/veiculo?auth_token=3674d78d158b6b252fd99074bf759aa0e7208b78>

```json
{
    "garagem": 2,
    "tipo": "M",
    "modelo": "Triumph",
    "cor": "Cinza",
    "ano": 2020
} 
```

# Observações finais

Ainda existem alguns bugs e falhas de integridade que estão ocorrendo em casos específicos, mas que podem ser corrigidos com tempo de desenvolvimento.

Das atividades relacionadas na [especificação](../blob/master/docs/Teste software engineer Franq.pdf) foram desenvolvidas total ou parcialmente os itens:
 
* Totalmente: 1, 2, 4, 7, 8, 10
* Parcialmente: 3, 5, 9
* Não desenvolvidas: 6
